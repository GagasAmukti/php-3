<?php
require 'animal.php';
require 'ape.php';
require 'frog.php';

//release 0
$sheep = new animal("shaun");

echo "<br>Name : " . $sheep->name . "<br>";
echo "Legs : " . $sheep->legs . "<br>";
echo "cold blooded : " . $sheep->cold_blooded . "<br>";

//release 1
$sungokong = new Ape("kera sakti");

echo "<br>Name : " . $sungokong->name . "<br>";
echo "Legs : " . $sungokong->legs . "<br>";
echo "cold blooded : " . $sungokong->cold_blooded . "<br>";
$sungokong->yell(); // "Auooo"
//

$kodok = new Frog("buduk");
echo "<br><br>Name : " . $kodok->name . "<br>";
echo "Legs : " . $kodok->legs . "<br>";
echo "cold blooded : " . $kodok->cold_blooded . "<br>"; //True karena katak berdarah dingin mas 
$kodok->jump();
echo "<br>Cold blooded saya ganti karena di gugel menyatakan katak benar berdarah dingin";
//

?>